#!/bin/bash

filename=$1
buildpathparam=${2%/}
mockInclude=$3

[ -z "$filename" ] && { echo "ERROR: missing param 1 to mmock"; exit 1; }
[ -z "$buildpathparam" ] && { echo "ERROR: missing param 2 to mmock"; exit 1; }
[ -z "$mockInclude" ] && { echo "ERROR: missing param 3 to mmock"; exit 1; }

if ! [ -d "$buildpathparam" ]; then
    mkdir -p $buildpathparam
fi

if [ -e $filename ] && [ -d "$buildpathparam" ]; then
    directory=$(dirname $filename)
    header=$(basename $filename)

    extension="${mockInclude##*.}"
    className="${header%.*}"
    mockName=$className"Mock"
    mockHeader=${mockName^^}"_"${extension^^}

    mockedInclude=$(echo $mockInclude | sed 's/Mock//g')

    destinationFile="$buildpathparam/$mockName.$extension"

    echo "Generating $destinationFile from $filename"

    echo "#ifndef $mockHeader" > $destinationFile
    echo "#define $mockHeader" >> $destinationFile
    printf "\n" >> $destinationFile
    echo "#include \"gmock/gmock.h\"" >> $destinationFile
    echo "#include \"$mockedInclude\"" >> $destinationFile
    printf "\n" >> $destinationFile
    
    #the python script creates the mock in an unconvenient name format, so we replace the name
    python $GMOCK_DIR/scripts/generator/gmock_gen.py $filename  >> $destinationFile
    className=$(grep "^class" $destinationFile | sed 's/.*public \(.*\) {/\1/') 
    wrongMockName="Mock"$className
    sed -i "s/$wrongMockName/${className}Mock/gI" $destinationFile
    #the python script creates the mock with weird spacing that makes it hard to read, so we clean
    sed -i 'N;s/\n      / /' $destinationFile

    printf "\n" >> $destinationFile
    echo "#endif // $mockHeader" >> $destinationFile

else
    echo "Usage : mmock Path/To/class_to_mock.h Path/To/BuildDirectory"
fi
